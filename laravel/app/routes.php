<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('about-us', ['as' => 'about', 'uses' => 'NosController@index']);

Route::get('portfolio/{categoria_slug?}', [
    'as'   => 'portfolio',
    'uses' => 'CatalogoController@index'
]);
Route::get('portfolio/{categoria_slug}/{filme_slug}', [
    'as'   => 'portfolio.show',
    'uses' => 'CatalogoController@show'
]);

Route::get('clients', ['as' => 'clients', 'uses' => 'ClientesController@index']);

Route::get('news', ['as' => 'news', 'uses' => 'NovidadesController@index']);
Route::get('news/{slug}', ['as' => 'news.show', 'uses' => 'NovidadesController@show']);

Route::get('contact', ['as' => 'contact', 'uses' => 'ContatoController@index']);
Route::post('contact', ['as' => 'contact.send', 'uses' => 'ContatoController@send']);


// Painel

Route::get('painel', [
    'before' => 'auth',
        'as' => 'painel.home',
      'uses' => 'Painel\HomeController@index'
]);

Route::get('painel/login', ['as' => 'painel.login', 'uses' => 'Painel\HomeController@login']);
Route::post('painel/login', ['as' => 'painel.auth', 'uses' => 'Painel\HomeController@attempt']);
Route::get('painel/logout', ['as' => 'painel.logout', 'uses' => 'Painel\HomeController@logout']);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::resource('banners', 'Painel\BannersController');
    Route::resource('cabecalhos', 'Painel\CabecalhosController');
    Route::resource('nos', 'Painel\NosController');
    Route::resource('catalogo', 'Painel\CatalogoCategoriasController');
    Route::resource('catalogo.filmes', 'Painel\CatalogoFilmesController');
    Route::resource('clientes', 'Painel\ClientesController');
    Route::resource('novidades', 'Painel\NovidadesController');
    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::post('ajax/order', 'Painel\AjaxController@order');
});
