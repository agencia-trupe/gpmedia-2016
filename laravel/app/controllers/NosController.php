<?php

use \Nos, \CatalogoFilme;

class NosController extends BaseController {

    public function index()
    {
        $nos    = Nos::first();
        $filmes = CatalogoFilme::posters()->get();

        return $this->view('frontend.nos', compact('nos', 'filmes'));
    }

}
