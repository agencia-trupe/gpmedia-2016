<?php

namespace Painel;

use \Cliente, \Input, \Session, \Redirect, \Validator, \CropImage;

class ClientesController extends BasePainelController {

    private $validation_rules = [
        'nome'   => 'required',
        'imagem' => 'required|image'
    ];

    public function index()
    {
        $clientes = Cliente::ordenados()->get();

        return $this->view('painel.clientes.index', compact('clientes'));
    }

    public function create()
    {
        return $this->view('painel.clientes.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', [
                'width'  => 250,
                'height' => 250,
                'path'   => 'assets/img/clientes/'
            ]);
            Cliente::create($input);
            Session::flash('sucesso', 'Cliente criado com sucesso.');

            return Redirect::route('painel.clientes.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar cliente.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);

        return $this->view('painel.clientes.edit', compact('cliente'));
    }

    public function update($id)
    {
        $cliente = Cliente::findOrFail($id);
        $input   = Input::all();

        $this->validation_rules['imagem'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', [
                    'width'  => 250,
                    'height' => 250,
                    'path'   => 'assets/img/clientes/'
                ]);
            } else {
                unset($input['imagem']);
            }

            $cliente->update($input);
            Session::flash('sucesso', 'Cliente alterado com sucesso.');

            return Redirect::route('painel.clientes.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar cliente.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Cliente::destroy($id);
            Session::flash('sucesso', 'Cliente removido com sucesso.');

            return Redirect::route('painel.clientes.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover cliente.']);

        }
    }

}