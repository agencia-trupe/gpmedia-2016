<?php

namespace Painel;

use \CatalogoCategoria, \View, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class CatalogoCategoriasController extends BasePainelController {

    protected $validation_rules = [
        'titulo' => 'required'
    ];

    public function index()
    {
        $categorias = CatalogoCategoria::ordenados()->get();

        return $this->view('painel.catalogo.index', compact('categorias'));
    }

    public function create()
    {
        return $this->view('painel.catalogo.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            CatalogoCategoria::create($input);
            Session::flash('sucesso', 'Categoria criada com sucesso.');

            return Redirect::route('painel.catalogo.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar categoria. Verifique se já existe outra categoria com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $categoria = CatalogoCategoria::findOrFail($id);

        return $this->view('painel.catalogo.edit', compact('categoria'));
    }

    public function update($id)
    {
        $categoria = CatalogoCategoria::findOrFail($id);
        $input     = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            $categoria->update($input);
            Session::flash('sucesso', 'Categoria alterada com sucesso.');

            return Redirect::route('painel.catalogo.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar categoria. Verifique se já existe outra categoria com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            CatalogoCategoria::destroy($id);
            Session::flash('sucesso', 'Categoria removida com sucesso.');

            return Redirect::route('painel.catalogo.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover categoria.']);

        }
    }

}