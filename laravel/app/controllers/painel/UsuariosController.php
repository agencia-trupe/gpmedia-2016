<?php

namespace Painel;

use \User, \View, \Input, \Session, \Redirect, \Hash;

class UsuariosController extends BasePainelController {

    public function index()
    {
        $usuarios = User::all();

        return $this->view('painel.usuarios.index', compact('usuarios'));
    }

    public function create()
    {
        return $this->view('painel.usuarios.create');
    }

    public function store()
    {
        $input = Input::all();

        if (!$input['username'] ||
            !$input['password'] ||
            ($input['password'] != $input['password_confirm']))
        {
            return Redirect::back()
                ->withErrors(['Erro ao criar usuário. Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada.'])
                ->withInput();
        }

        try {

            $input['password'] = Hash::make($input['password']);

            User::create($input);
            Session::flash('sucesso', 'Usuário criado com sucesso.');

            return Redirect::route('painel.usuarios.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar usuário. Verifique se o nome de usuário ou o email não estão sendo utilizados.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $usuario = User::findOrFail($id);

        return $this->view('painel.usuarios.edit', compact('usuario'));
    }

    public function update($id)
    {
        $input = Input::all();
        $user  = User::findOrFail($id);

        if ($input['password'])
        {
            if ($input['password'] == $input['password_confirm']) {

                $input['password'] = Hash::make($input['password']);

            } else {

                return Redirect::back()
                ->withErrors(['Erro ao alterar usuário. Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada.'])
                ->withInput();

            }
        }

        if (!$input['username'])
        {
            return Redirect::back()
                ->withErrors(['Erro ao alterar usuário. Verifique se os campos estão devidamente preenchidos e que a confirmação de senha é igual à senha informada.'])
                ->withInput();
        }

        try {

            $user->update($input);
            Session::flash('sucesso', 'Usuário alterado com sucesso.');

            return Redirect::route('painel.usuarios.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar usuário. Verifique se o nome de usuário ou o email não estão sendo utilizados.'])
                ->withInput();

        }
    }

    public function destroy($id) {
        try {

            User::destroy($id);
            Session::flash('sucesso', 'Usuário removido com sucesso.');

            return Redirect::route('painel.usuarios.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover usuário.']);

        }
    }

}