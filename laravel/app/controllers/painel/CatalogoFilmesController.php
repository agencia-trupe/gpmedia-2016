<?php

namespace Painel;

use \CatalogoFilme, \CatalogoCategoria, \View, \Input, \Session, \Redirect, \Validator, \Str, \CropImage;

class CatalogoFilmesController extends BasePainelController {

    protected $validation_rules = [
        'titulo'        => 'required',
        'olho'          => 'required',
        'descricao'     => 'required',
        'ficha_tecnica' => 'required',
        'poster'        => 'required|image',
        'capa'          => 'required|image',
    ];

    protected $image_config = [
        'capa' => [
            'width'  => 675,
            'height' => 450,
            'path'   => 'assets/img/catalogo/'
        ],
        'poster' => [
            'width'  => 340,
            'height' => 510,
            'path'   => 'assets/img/catalogo/poster/'
        ]
    ];

    public function index($categoria_id)
    {
        $categoria = CatalogoCategoria::findOrFail($categoria_id);
        $filmes    = CatalogoFilme::categoria($categoria_id)->ordenados()->get();

        return $this->view('painel.catalogo.filmes.index', compact(['filmes', 'categoria']));
    }

    public function create($categoria_id)
    {
        $categoria = CatalogoCategoria::findOrFail($categoria_id);

        return $this->view('painel.catalogo.filmes.create', compact('categoria'));
    }

    public function store($categoria_id)
    {
        $categoria = CatalogoCategoria::findOrFail($categoria_id);
        $input     = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['categoria_id'] = $categoria->id;

            $input['poster'] = CropImage::make('poster', $this->image_config['poster']);
            $input['capa']   = CropImage::make('capa', $this->image_config['capa']);

            if (Input::has('mostrar_home')) {
                $input['mostrar_home'] = true;
            } else {
                $input['mostrar_home'] = false;
            }

            CatalogoFilme::create($input);
            Session::flash('sucesso', 'Filme criado com sucesso.');

            return Redirect::route('painel.catalogo.filmes.index', $categoria->id);

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar filme. Verifique se já existe outro filme com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($categoria_id, $filme_id)
    {
        $categoria = CatalogoCategoria::findOrFail($categoria_id);
        $filme     = CatalogoFilme::findOrFail($filme_id);

        return $this->view('painel.catalogo.filmes.edit', compact(['categoria', 'filme']));
    }

    public function update($categoria_id, $filme_id)
    {
        $categoria = CatalogoCategoria::findOrFail($categoria_id);
        $filme     = CatalogoFilme::findOrFail($filme_id);
        $input     = Input::all();

        $this->validation_rules['poster'] = 'image';
        $this->validation_rules['capa'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['categoria_id'] = $categoria->id;

            if (Input::hasFile('poster')) {
                $input['poster'] = CropImage::make('poster', $this->image_config['poster']);
            } else {
                unset($input['poster']);
            }

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config['capa']);
            } else {
                unset($input['capa']);
            }

            if (Input::has('mostrar_home')) {
                $input['mostrar_home'] = true;
            } else {
                $input['mostrar_home'] = false;
            }

            $filme->update($input);
            Session::flash('sucesso', 'Filme alterado com sucesso.');

            return Redirect::route('painel.catalogo.filmes.index', $categoria->id);

        } catch (\Exception $e) {

            return $e;

            return Redirect::back()
                ->withErrors(['Erro ao alterar filme. Verifique se já existe outro filme com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($categoria_id, $filme_id)
    {
        $categoria = CatalogoCategoria::findOrFail($categoria_id);

        try {

            CatalogoFilme::destroy($filme_id);
            Session::flash('sucesso', 'Filme removido com sucesso.');

            return Redirect::route('painel.catalogo.filmes.index', $categoria_id);

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover filme.']);

        }
    }

}