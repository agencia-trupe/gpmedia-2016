<?php

namespace Painel;

use \Nos, \Input, \Validator, \Redirect, \Session;

class NosController extends BasePainelController {

    private $validation_rules = [
        'texto' => 'required'
    ];

    public function index()
    {
        $nos = Nos::first();

        return $this->view('painel.nos.index', compact('nos'));
    }

    public function update($id)
    {
        $nos   = Nos::findOrFail($id);
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $nos->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.nos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}