<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'twitter' => 'http://twitter.com',
                'facebook' => 'http://facebook.com',
                'vimeo' => 'http://vimeo.com',
                'youtube' => 'http://youtube.com',
                'instagram' => 'http://instagram.com',
                'telefones' => '(+55 11) 3812 6833, (+55 11) 3814 3958',
                'contato' => 'Grabriel Rohonyi / Chun Kim',
                'email' => 'comercial@grandprixmedia.net',
                'endereco' => 'Rua Professor Artur Ramos, 183,<br>cj. 52, 01454-011, São Paulo - Brazil',
                'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.6235393119064!2d-46.686003099999986!3d-23.581960499999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce576bb86c4747%3A0xd9250347fd6eea15!2sR.+Prof.+Artur+Ramos%2C+183+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP%2C+01454-011!5e0!3m2!1spt-BR!2sbr!4v1426018055376" width="100%" height="100%" frameborder="0" style="border:0"></iframe>'
            )
        );

        DB::table('contato')->insert($data);
    }

}