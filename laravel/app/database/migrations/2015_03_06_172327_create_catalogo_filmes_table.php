<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogoFilmesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('catalogo_filmes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('categoria_id');
			$table->string('titulo');
			$table->string('slug')->unique();
			$table->text('olho');
			$table->text('descricao');
			$table->text('ficha_tecnica');
			$table->string('poster');
			$table->string('capa');
			$table->string('embed_site')->default('vimeo');
			$table->string('embed_video');
			$table->boolean('mostrar_home')->default(0);
			$table->integer('ordem')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('catalogo_filmes');
	}

}
