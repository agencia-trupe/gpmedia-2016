@section('content')

    <legend>
        <h2><small>{{ $categoria->titulo }} /</small> Adicionar Filme</h2>
    </legend>

    {{ Form::open(['route' => ['painel.catalogo.filmes.store', $categoria->id], 'files' => true]) }}

        @include('painel.catalogo.filmes._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop