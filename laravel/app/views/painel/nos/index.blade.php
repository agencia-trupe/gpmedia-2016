@section('content')

    <legend>
        <h2>Nós</h2>
    </legend>

    {{ Form::model($nos, [
        'route' => ['painel.nos.update', $nos->id],
        'method' => 'patch'])
    }}

        @include('painel.nos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop