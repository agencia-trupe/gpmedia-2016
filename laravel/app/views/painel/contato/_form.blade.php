@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ HTML::decode(Form::label('telefones', 'Telefones <small>(separados por vírgula / DDD entre parênteses)</small>')) }}
    {{ Form::text('telefones', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('contato', 'Contato') }}
    {{ Form::text('contato', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('email', 'E-mail') }}
    {{ Form::email('email', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('endereco', 'Endereço') }}
    {{ Form::text('endereco', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('twitter', 'Twitter') }}
    {{ Form::text('twitter', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('facebook', 'Facebook') }}
    {{ Form::text('facebook', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('vimeo', 'Vimeo') }}
    {{ Form::text('vimeo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('youtube', 'YouTube') }}
    {{ Form::text('youtube', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('instagram', 'Instagram') }}
    {{ Form::text('instagram', null, ['class' => 'form-control']) }}
</div>

<hr>

<div class="form-group">
    {{ Form::label('googlemaps', 'Código Google Maps') }}
    {{ Form::textarea('googlemaps', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}