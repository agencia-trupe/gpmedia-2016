@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="well form-group">
    {{ Form::label('nos', 'Nós') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/cabecalhos/'.$cabecalhos->nos) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('nos', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('catalogo', 'Catálogo') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/cabecalhos/'.$cabecalhos->catalogo) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('catalogo', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('clientes', 'Clientes') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/cabecalhos/'.$cabecalhos->clientes) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('clientes', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('novidades', 'Novidades') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/cabecalhos/'.$cabecalhos->novidades) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('novidades', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('contato', 'Contato') }}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/cabecalhos/'.$cabecalhos->contato) }}" style="display:block; margin-bottom: 10px; max-width:600px;height:auto;">
@endif
    {{ Form::file('contato', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}