@section('content')

    <legend>
        <h2>Imagens de Cabeçalho <small>(1920x245px)</small></h2>
    </legend>

    {{ Form::model($cabecalhos, [
        'route' => ['painel.cabecalhos.update', $cabecalhos->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.cabecalhos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop