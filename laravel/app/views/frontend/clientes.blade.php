@section('content')
@section('content')
@include('frontend.common._title', [
    'title' => 'Clients',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->clientes)
])

    <main id="clientes">
        <div class="center">
            @foreach($clientes as $cliente)
                <div class="cliente">
                    <img src="{{ url('assets/img/clientes/'.$cliente->imagem) }}" alt="{{ $cliente->nome }}" title="{{ $cliente->nome }}">
                </div>
            @endforeach
        </div>
    </main>
@stop