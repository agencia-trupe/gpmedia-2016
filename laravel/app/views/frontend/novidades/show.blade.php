@section('content')
@include('frontend.common._title', [
    'title' => 'News',
    'image' => url('assets/img/cabecalhos/'.$cabecalho->novidades)
])

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <main id="novidades" class="show">
        <div class="center">
            <div class="topo">
                <p class="data">{{ \Tools::formatData($novidade->data) }}</p>
                <div class="social-share">
                    <div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                    <a href="https://twitter.com/share" class="twitter-share-button" data-text="{{ $novidade->titulo }}">Tweet</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
                </div>
            </div>
            @if($novidade->capa)
            <div class="capa">
                <img src="{{ url('assets/img/novidades/capa/'.$novidade->capa) }}" alt="">
            </div>
            @endif
            <div class="texto @if(!$novidade->embed_video)full-width @endif">
                <h2>{{ $novidade->titulo }}</h2>
                {{ $novidade->texto }}
            </div>
            @if($novidade->embed_video)
            <div class="embed">
                <div class="embed-video">
                    @if($novidade->embed_site == 'vimeo')
                    <iframe src="//player.vimeo.com/video/{{ $novidade->embed_video }}?color=8bc17a&portrait=0&badge=0&title=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen autoplay="0"></iframe>
                    @elseif($novidade->embed_site == 'youtube')
                    <iframe src="http://www.youtube.com/embed/{{ $novidade->embed_video }}" frameborder="0"/></iframe>
                    @endif
                </div>
            </div>
            @endif
            <a href="{{ route('news') }}" class="voltar">back</a>
        </div>
    </main>
@stop