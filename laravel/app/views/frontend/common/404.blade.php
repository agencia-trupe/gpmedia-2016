@extends('frontend.common.template')
@section('content')

    <main class="not-found">
        <div class="center">
            <h1>Page not found</h1>
        </div>
    </main>

@stop