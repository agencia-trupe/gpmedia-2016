<?php

class CatalogoFilme extends Eloquent
{

    protected $table = 'catalogo_filmes';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->whereCategoriaId($categoria_id);
    }

    public function scopeHome($query, $limit = 5)
    {
        return $query->whereMostrarHome(1)->orderBy(DB::raw('RAND()'))->take($limit);
    }

    public function scopePosters($query, $limit = 5)
    {
        return $query->select(['titulo', 'poster'])->orderBy(DB::raw('RAND()'))->take($limit);
    }

    public function categoria_parent()
    {
        return $this->belongsTo('CatalogoCategoria', 'categoria_id');
    }

}
