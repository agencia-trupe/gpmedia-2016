<?php

class Cliente extends Eloquent
{

    protected $table = 'clientes';

    protected $hidden = [];

    protected $fillable = ['nome', 'imagem'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
