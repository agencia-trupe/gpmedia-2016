<?php

class CatalogoCategoria extends Eloquent
{

    protected $table = 'catalogo_categorias';

    protected $hidden = [];

    protected $fillable = ['titulo', 'slug'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function filmes()
    {
        return $this->hasMany('CatalogoFilme', 'categoria_id');
    }

}
